package no.ntnu.idatt2001.runarin.cardgame.backend;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;

public class DeckOfCardsTest {

    @Test
    public void instantiateClassAndAssertThatFiftyTwoUniqueCardsAreInDeck() {
        /*
        Test that asserts the content of the generated deck of cards.
         */
        DeckOfCards deck = new DeckOfCards();
        ArrayList<PlayingCard> cards = deck.getDeck();

        String suit;
        String face;
        for (int i = 0; i < 52; i++) {
            if (i < 13) {
                suit = "S";
                face = String.valueOf(i + 1);
            }
            else if (i < 26) {
                suit = "D";
                face = String.valueOf(i - 13 + 1);
            }
            else if (i < 39) {
                suit = "H";
                face = String.valueOf(i - 26 + 1);
            }
            else {
                suit = "C";
                face = String.valueOf(i - 39 + 1);
            }
            assertEquals(suit+face, cards.get(i).getAsString());
        }
    }

    @Test
    public void assertTheSizeOfDeckOfCardsToBeFiftyTwo() {
        /*
        Test that asserts the size of the generated deck of cards to be fifty-two.
         */
        DeckOfCards deck = new DeckOfCards();
        assertEquals(52, deck.getDeck().size());
    }

    @Test
    public void assertThatGetDeckMethodReturnsADeepCopy() {
        /*
        Test asserts that the getDeck()-method returns a
        unique deep copy of the original deck.
         */
        DeckOfCards deck = new DeckOfCards();
        ArrayList<PlayingCard> deckOne = deck.getDeck();
        ArrayList<PlayingCard> deckTwo = deck.getDeck();

        assertNotEquals(deckOne.hashCode(), deckTwo.hashCode());
        assertNotEquals(deckOne.get(0).hashCode(), deckTwo.get(0).hashCode());
    }

    @Test
    public void dealHandMethodGivesOnlyTheGivenAmountOfCards() {
        /*
        Test asserts that the method only deals the given amount of cards to a hand.
         */
        DeckOfCards deck = new DeckOfCards();

        ArrayList<PlayingCard> hand = deck.dealHand(5);

        assertEquals(5, hand.size());
        assertFalse(hand.size() > 5);
        assertFalse(hand.size() < 5);
    }

    @Test
    public void methodDealHandThrowsIllegalArgumentExceptionWhenNegativeNumber() {
        /*
        Test asserts IllegalArgumentException is thrown from
        method if a negative number is given as parameter.
         */
        try {
            DeckOfCards deck = new DeckOfCards();
            ArrayList<PlayingCard> hand = deck.dealHand(-1);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Cannot deal a negative number of cards to hand.",
                    e.getMessage());
        }
    }

    @Test
    public void methodDealHandThrowsIllegalArgumentExceptionWhenNumberOfCardsDealtIsZero() {
        /*
        Test asserts IllegalArgumentException is thrown from
        method if zero is given as parameter.
         */
        try {
            DeckOfCards deck = new DeckOfCards();
            ArrayList<PlayingCard> hand = deck.dealHand(0);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Must deal at least one card to hand when using this method.",
                    e.getMessage());
        }
    }

    @Test
    public void methodDealHandThrowsIllegalArgumentExceptionWhenNumberOfCardsDealtAreMoreThanInDeck() {
        /*
        Test asserts IllegalArgumentException is thrown from
        method if the number of cards dealt are over the amount of cards in deck.
         */
        try {
            DeckOfCards deck = new DeckOfCards();
            ArrayList<PlayingCard> hand = deck.dealHand(53);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Cannot deal more cards than what is in the deck.",
                    e.getMessage());
        }
    }
}
