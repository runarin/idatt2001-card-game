package no.ntnu.idatt2001.runarin.cardgame.backend;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

    @Test
    public void instantiateClassAndAssertGettersAndToString() {
        /*
        Test that asserts the correct getters are returned.
         */
        PlayingCard testCard = new PlayingCard("H",4);

        assertEquals("H", testCard.getSuit());
        assertEquals(4, testCard.getFace());
        assertEquals("H4", testCard.getAsString());
    }

    @Test
    public void assertWrongSuitInputThrowsIllegalArgumentException() {
        /*
        Test that asserts that an Illegal Argument Exception is
        thrown if the wrong suit is given to the constructor.
         */
        try {
            PlayingCard testCard = new PlayingCard("F", 4);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Suit must be either 'S', 'H', 'D' or 'C'.", e.getMessage());
        }
    }

    @Test
    public void assertFaceInputBelowZeroThrowsIllegalArgumentException() {
        /*
        Test that asserts that an Illegal Argument Exception is
        thrown if the wrong face is given to the constructor.
         */
        try {
            PlayingCard testCard = new PlayingCard("D",0);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Face must be within range of 1 and 13, inclusive.", e.getMessage());
        }
    }

    @Test
    public void assertFaceInputAboveThirteenThrowsIllegalArgumentException() {
        /*
        Test that asserts that an Illegal Argument Exception is
        thrown if the wrong face is given to the constructor.
         */
        try {
            PlayingCard testCard = new PlayingCard("D",14);
            fail();
        } catch (IllegalArgumentException e) {
            assertEquals("Face must be within range of 1 and 13, inclusive.", e.getMessage());
        }
    }
}
