package no.ntnu.idatt2001.runarin.cardgame.backend;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {

    @Test
    public void assertInstantiationAddingAndGettingHand() {
        /*
        Test asserts that class can be instantiated and the list
        can add cards, as well as the getting the list of cards.
         */
        HandOfCards hand = new HandOfCards();
        hand.addCardToHand(new PlayingCard("H", 4));
        hand.addCardToHand(new PlayingCard("H", 5));

        assertEquals("H4", hand.getHand().get(0).getAsString());
        assertEquals("H5", hand.getHand().get(1).getAsString());
        assertEquals(2, hand.getHand().size());
    }

    @Test
    public void assertAlreadyExistingCardCannotBeAddedToHand() {
        /*
        Test asserts that an already existing card cannot be added to hand.
         */
        HandOfCards hand = new HandOfCards();
        hand.addCardToHand(new PlayingCard("H", 4));
        hand.addCardToHand(new PlayingCard("H", 4));

        assertEquals(1, hand.getHand().size());
        assertFalse(hand.getHand().size() > 1);
    }

    @Test
    public void methodReceiveNewHandDeletesOldHandAndAddsNewCardsToList() {
        /*
        Test asserts that receiveNewHand()-method adds list of cards and also
        clears the list when running the method again and adding an empty list.
         */
        HandOfCards hand = new HandOfCards();
        ArrayList<PlayingCard> deal = new ArrayList<>();
        deal.add(new PlayingCard("H",1));
        deal.add(new PlayingCard("S",2));
        deal.add(new PlayingCard("C",3));

        hand.receiveNewHand(deal);

        assertEquals(3, hand.getHand().size());
        assertEquals("H1", hand.getHand().get(0).getAsString());
        assertEquals("S2", hand.getHand().get(1).getAsString());
        assertEquals("C3", hand.getHand().get(2).getAsString());

        hand.receiveNewHand(new ArrayList<>());

        assertTrue(hand.getHand().isEmpty());
    }

    @Test
    public void assertSumOfFacesIsReturned() {
        /*
        Test asserts that filtering by face returns a list with only the given face.
         */
        HandOfCards hand = new HandOfCards();
        hand.addCardToHand(new PlayingCard("H", 7));
        hand.addCardToHand(new PlayingCard("H", 5));
        hand.addCardToHand(new PlayingCard("S", 5));

        assertEquals("17", hand.sumOfFaces());
    }

    @Test
    public void assertFilteringHandToOnlyAGivenSuit() {
        /*
        Test asserts that filtering by suit returns a list with only the given suit.
         */
        HandOfCards hand = new HandOfCards();
        hand.addCardToHand(new PlayingCard("H", 7));
        hand.addCardToHand(new PlayingCard("H", 5));
        hand.addCardToHand(new PlayingCard("S", 5));
        hand.addCardToHand(new PlayingCard("S", 7));
        hand.addCardToHand(new PlayingCard("C", 7));

        ArrayList<PlayingCard> filteredHand = hand.listOfSuit("H");

        assertEquals(2, filteredHand.size());
        assertEquals("H7", filteredHand.get(0).getAsString());
        assertEquals("H5", filteredHand.get(1).getAsString());
    }

    @Test
    public void assertsQueenOfSpadesIsOnHandOrNot() {
        /*
        Test asserts that a queen of spades is on hand only when added.
         */
        HandOfCards hand = new HandOfCards();
        hand.addCardToHand(new PlayingCard("H", 7));
        hand.addCardToHand(new PlayingCard("H", 5));
        assertFalse(hand.hasQueenOfSpades());

        hand.addCardToHand(new PlayingCard("S", 12));
        assertTrue(hand.hasQueenOfSpades());
    }


    @Test
    public void assertsThatIsFiveCardFlushMethodReturnsTrueOnlyWhenFiveOrMoreCardsAreOfTheSameSuit() {
        /*
        Test asserts that the isFiveCardFlush-method() only returns
        true when five or more cards are of the same suit.
         */
        HandOfCards handOne = new HandOfCards();
        handOne.addCardToHand(new PlayingCard("H", 7));
        handOne.addCardToHand(new PlayingCard("H", 5));
        handOne.addCardToHand(new PlayingCard("S", 5));
        handOne.addCardToHand(new PlayingCard("S", 7));
        handOne.addCardToHand(new PlayingCard("C", 7));
        assertFalse(handOne.isFiveCardFlush());

        HandOfCards handTwo = new HandOfCards();
        handTwo.addCardToHand(new PlayingCard("H", 1));
        handTwo.addCardToHand(new PlayingCard("H", 2));
        handTwo.addCardToHand(new PlayingCard("H", 3));
        handTwo.addCardToHand(new PlayingCard("H", 4));
        assertFalse(handTwo.isFiveCardFlush()); // Only four cards of the same suit gives false.

        handTwo.addCardToHand(new PlayingCard("H", 5));
        assertTrue(handTwo.isFiveCardFlush());  // Five cards of the same suit gives true.

        handTwo.addCardToHand(new PlayingCard("H", 6));
        assertTrue(handTwo.isFiveCardFlush());  // Six cards of the same suit gives true.
    }

    @Test
    public void assertsThatIsFourCardFlushMethodReturnsTrueOnlyWhenFourCardsAreOfTheSameSuit() {
        /*
        Test asserts that the isFourCardFlush-method() only returns
        true when four cards are of the same suit.
         */
        HandOfCards handOne = new HandOfCards();
        handOne.addCardToHand(new PlayingCard("H", 7));
        handOne.addCardToHand(new PlayingCard("H", 5));
        handOne.addCardToHand(new PlayingCard("S", 5));
        handOne.addCardToHand(new PlayingCard("S", 7));
        handOne.addCardToHand(new PlayingCard("C", 7));
        assertFalse(handOne.isFourCardFlush());

        HandOfCards handTwo = new HandOfCards();
        handTwo.addCardToHand(new PlayingCard("H", 1));
        handTwo.addCardToHand(new PlayingCard("H", 2));
        handTwo.addCardToHand(new PlayingCard("H", 3));
        assertFalse(handTwo.isFourCardFlush()); // Three cards of the same suit gives false.

        handTwo.addCardToHand(new PlayingCard("H", 4));
        assertTrue(handTwo.isFourCardFlush()); // Four cards of the same suit gives true.

        handTwo.addCardToHand(new PlayingCard("H", 5));
        assertFalse(handTwo.isFourCardFlush());  // Five cards of the same suit gives false.
    }

    @Test
    public void assertsThatIsThreeCardFlushMethodReturnsTrueOnlyWhenThreeCardsAreOfTheSameSuit() {
        /*
        Test asserts that the isThreeCardFlush-method() only returns
        true when three cards are of the same suit.
         */
        HandOfCards handOne = new HandOfCards();
        handOne.addCardToHand(new PlayingCard("H", 7));
        handOne.addCardToHand(new PlayingCard("H", 5));
        handOne.addCardToHand(new PlayingCard("S", 5));
        handOne.addCardToHand(new PlayingCard("S", 7));
        handOne.addCardToHand(new PlayingCard("C", 7));
        assertFalse(handOne.isThreeCardFlush());

        HandOfCards handTwo = new HandOfCards();
        handTwo.addCardToHand(new PlayingCard("H", 1));
        handTwo.addCardToHand(new PlayingCard("H", 2));
        assertFalse(handTwo.isThreeCardFlush()); // Two cards of the same suit gives false.

        handTwo.addCardToHand(new PlayingCard("H", 3));
        assertTrue(handTwo.isThreeCardFlush()); // Three cards of the same suit gives true.

        handTwo.addCardToHand(new PlayingCard("H", 4));
        assertFalse(handTwo.isThreeCardFlush()); // Four cards of the same suit gives false.
    }

    @Test
    public void assertsThatIsStraightMethodReturnsTrueWhenFiveConsecutiveCardsAreGiven() {
        /*
        Test asserts that the isStraight-method() only returns
        true when five consecutive cards are given.
         */
        HandOfCards handOne = new HandOfCards();
        handOne.addCardToHand(new PlayingCard("H", 7));
        handOne.addCardToHand(new PlayingCard("H", 5));
        handOne.addCardToHand(new PlayingCard("S", 5));
        handOne.addCardToHand(new PlayingCard("S", 7));
        handOne.addCardToHand(new PlayingCard("C", 7));
        assertFalse(handOne.isStraight());

        HandOfCards handTwo = new HandOfCards();
        handTwo.addCardToHand(new PlayingCard("H", 3));
        handTwo.addCardToHand(new PlayingCard("H", 4));
        handTwo.addCardToHand(new PlayingCard("S", 5));
        handTwo.addCardToHand(new PlayingCard("S", 7));
        handTwo.addCardToHand(new PlayingCard("C", 7));
        assertFalse(handTwo.isStraight());

        HandOfCards handThree = new HandOfCards();
        handThree.addCardToHand(new PlayingCard("H", 3));
        handThree.addCardToHand(new PlayingCard("H", 4));
        handThree.addCardToHand(new PlayingCard("S", 5));
        handThree.addCardToHand(new PlayingCard("S", 6));
        handThree.addCardToHand(new PlayingCard("C", 7));
        assertTrue(handThree.isStraight());
    }

    @Test
    public void assertMethodIsStraightFlushReturnsExpectedBoolean() {
        /*
        Test asserts that the isStraightFlush-method() only returns
        true when five consecutive cards within the same suit are given.
         */
        HandOfCards handOne = new HandOfCards();
        handOne.addCardToHand(new PlayingCard("S", 3));
        handOne.addCardToHand(new PlayingCard("S", 4));
        handOne.addCardToHand(new PlayingCard("S", 5));
        handOne.addCardToHand(new PlayingCard("S", 6));
        handOne.addCardToHand(new PlayingCard("S", 8));
        assertFalse(handOne.isStraightFlush()); // Expect false as not straight.

        HandOfCards handTwo = new HandOfCards();
        handTwo.addCardToHand(new PlayingCard("H", 3));
        handTwo.addCardToHand(new PlayingCard("H", 4));
        handTwo.addCardToHand(new PlayingCard("H", 5));
        handTwo.addCardToHand(new PlayingCard("S", 6));
        handTwo.addCardToHand(new PlayingCard("H", 7));
        assertFalse(handTwo.isStraightFlush()); // Expect false as not flush.

        HandOfCards handThree = new HandOfCards();
        handThree.addCardToHand(new PlayingCard("D", 3));
        handThree.addCardToHand(new PlayingCard("D", 4));
        handThree.addCardToHand(new PlayingCard("D", 5));
        handThree.addCardToHand(new PlayingCard("D", 6));
        handThree.addCardToHand(new PlayingCard("D", 7));
        assertTrue(handThree.isStraightFlush()); // Expect true as flush and straight.
    }
}
