package no.ntnu.idatt2001.runarin.cardgame.frontend.model;

import no.ntnu.idatt2001.runarin.cardgame.backend.HandOfCards;
import no.ntnu.idatt2001.runarin.cardgame.backend.PlayingCard;

import java.util.ArrayList;

/**
 * Card Game Model-class.
 *
 * @author Runar Indahl
 * @version 1.0
 * @since 2022-04-01
 */
public class CardGameModel {

    /**
     * Method builds string to be shown in status window in the application.
     */
    public StringBuilder buildString(HandOfCards hand) {
        StringBuilder text = new StringBuilder();

        text.append("Sum of all faces: ").append(hand.sumOfFaces()).append("\n");

        ArrayList<PlayingCard> handOfHearts = hand.listOfSuit("H");
        if (!handOfHearts.isEmpty()) {
            text.append("Hearts:");
            for (PlayingCard card : handOfHearts) {
                text.append(" ").append(card.getAsString());
            } text.append("\n");
        } else {
            text.append("No hearts.\n");
        }

        if (hand.hasQueenOfSpades()) {
            text.append("Queen of Spades: Yes!\n");
        } else {
            text.append("Queen of Spades: No.\n");
        }

        if (hand.isStraightFlush()) {
            text.append("STRAIGHT FLUSH!\n");
        } else if (hand.isFiveCardFlush()) {
            text.append("Flush (5)!\n");
        } else if (hand.isFourCardFlush()) {
            text.append("Flush (4).\n");
        } else if (hand.isThreeCardFlush()) {
            text.append("Flush (3).\n");
        } else {
            text.append("No flush.\n");
        }

        if (hand.isStraight() & !hand.isStraightFlush()) {
            text.append("STRAIGHT!\n");
        }

        return text;
    }
}
