package no.ntnu.idatt2001.runarin.cardgame.backend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards. A hand can consist of any
 * number of cards up to the same amount as the deck of cards.
 *
 * @author Runar Indahl
 * @version 1.0
 * @since 2022-04-01
 */
public class HandOfCards {

    private final ArrayList<PlayingCard> hand;

    /**
     * Creates an instance of a HandOfCards,
     */
    public HandOfCards() {
        this.hand = new ArrayList<>();
    }

    /**
     * Returns the hand of cards.
     *
     * @return the card hand as an ArrayList.
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Adds a unique card to the hand.
     *
     * @param card PlayingCard to be added to hand.
     */
    public void addCardToHand(PlayingCard card) {
        if (hand.contains(card)) return;
        for (PlayingCard playingCard : hand)
            if (card.getAsString().equals(playingCard.getAsString())) return;
        hand.add(card);
    }

    /**
     * Adds a list of cards to the hand.
     *
     * @see DeckOfCards , cards can be given from method dealHand(n).
     * @param receivedHand list of cards to add to hand.
     */
    public void receiveNewHand(ArrayList<PlayingCard> receivedHand) {
        this.hand.clear();
        this.hand.addAll(receivedHand);
    }

    /**
     * Returns the sum of faces on hand.
     *
     * @return sum of all faces on hand in String format.
     */
    public String sumOfFaces() {
        return String.valueOf(hand.stream()
                .mapToInt(PlayingCard::getFace)
                .sum());
    }

    /**
     * Returns a filtered ArrayList from hand consisting only of suit.
     *
     * @param suit the suit to filter the hand by.
     * @return an ArrayList consisting of only the specified suit.
     */
    public ArrayList<PlayingCard> listOfSuit(String suit) {
        return hand.stream()
                .filter(c -> c.getSuit().contains(suit))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Returns boolean value for if a queen of spades is on hand or not.
     *
     * @return boolean value for if a queen of spades is on hand or not.
     */
    public boolean hasQueenOfSpades() {
        return hand.stream()
                .map(PlayingCard::getAsString)
                .anyMatch(q -> q.equals("S12"));
    }

    /**
     * Returns boolean value for if the hand contains a five card+ flush or not.
     *
     * @return true if the hand contains five or more cards of the same color, else false.
     */
    public boolean isFiveCardFlush() {
        return hand.stream()
                .map(PlayingCard::getSuit)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .anyMatch(n -> n.getValue() >= 5);
    }

    /**
     * Returns boolean value for if the hand contains a four card flush or not.
     *
     * @return true if the hand contains four cards of the same color, else false.
     */
    public boolean isFourCardFlush() {
        return hand.stream()
                .map(PlayingCard::getSuit)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .anyMatch(n -> n.getValue() == 4);
    }

    /**
     * Returns boolean value for if the hand contains a three card flush or not.
     *
     * @return true if the hand contains three cards of the same color, else false.
     */
    public boolean isThreeCardFlush() {
        return hand.stream()
                .map(PlayingCard::getSuit)
                .collect(Collectors.groupingByConcurrent(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .anyMatch(n -> n.getValue() == 3);
    }

    /**
     * Returns boolean value for if the hand contains a straight.
     * A straight is five consecutive cards by face.
     *
     * @return true if the hand contains a straight, else false.
     */
    public boolean isStraight() {
        Collections.sort(hand);
        int faceNr = hand.get(0).getFace();
        for (PlayingCard card : hand) {
            if (faceNr != card.getFace()) return false;
            faceNr++;
        } return true;
    }

    /**
     * Returns boolean for which the hand contains a straight flush.
     * A straight flush is five consecutive cards by face in the same suit.
     *
     * @return true if hand contains a straight flush, else false.
     */
    public boolean isStraightFlush() {
        return isFiveCardFlush() & isStraight();
    }
}
