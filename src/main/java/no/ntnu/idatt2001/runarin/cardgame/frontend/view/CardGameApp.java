package no.ntnu.idatt2001.runarin.cardgame.frontend.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * The view-class which launches the card game's main window/application.
 *
 * @author Runar Indahl
 * @version 1.0
 * @since 2022-04-01
 */
public class CardGameApp extends Application {

    private static final String VERSION = "1.0";

    /**
     * Method loads the card game window.
     *
     * @param stage stage to show the view.
     */
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader root = new FXMLLoader(
                CardGameApp.class.getClassLoader().getResource("card_game_view.fxml"));
        Scene scene = new Scene(root.load(),600,400);

        stage.setTitle("Card Game   v." + VERSION);

        Image icon = new Image("icon.png");
        stage.getIcons().add(icon);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main starting point of the application.
     *
     * @param args command line arguments provided during startup. Not used in this app.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
