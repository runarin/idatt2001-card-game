package no.ntnu.idatt2001.runarin.cardgame.frontend.controller;

import javafx.scene.control.Button;
import no.ntnu.idatt2001.runarin.cardgame.backend.DeckOfCards;
import no.ntnu.idatt2001.runarin.cardgame.backend.HandOfCards;
import no.ntnu.idatt2001.runarin.cardgame.frontend.model.CardGameModel;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Card game controller-class.
 *
 * @author Runar Indahl
 * @version 1.0
 * @since 2022-04-01
 */

public class CardGameController {

    private final CardGameModel model = new CardGameModel();
    private final HandOfCards hand = new HandOfCards();

    @FXML
    private Button btnCheckHand;

    @FXML
    private TextArea handStatusText;

    @FXML
    private ImageView cardOne = new ImageView();

    @FXML
    private ImageView cardTwo = new ImageView();

    @FXML
    private ImageView cardThree = new ImageView();

    @FXML
    private ImageView cardFour = new ImageView();

    @FXML
    private ImageView cardFive = new ImageView();

    /**
     * Sets new images to represents the cards on the hand.
     */
    public void updateImages() {
        cardOne.setImage(new Image(hand.getHand().get(0).getCardPathPNG()));
        cardTwo.setImage(new Image(hand.getHand().get(1).getCardPathPNG()));
        cardThree.setImage(new Image(hand.getHand().get(2).getCardPathPNG()));
        cardFour.setImage(new Image(hand.getHand().get(3).getCardPathPNG()));
        cardFive.setImage(new Image(hand.getHand().get(4).getCardPathPNG()));
    }

    /**
     * Deal five cards to the hand.
     *
     * @param event button "Deal hand" is clicked.
     */
    public void dealHand(ActionEvent event) {
        handStatusText.clear();
        btnCheckHand.setDisable(false);

        DeckOfCards deck = new DeckOfCards();
        hand.receiveNewHand(deck.dealHand(5));
        updateImages();
    }

    /**
     * Check through the hand of cards for special occurrences and returns descriptive String.
     * Special events are:
     *  1. Calculates the sum of faces.
     *  2. All suits of hearts.
     *  3. Spare Queen is in hand.
     *  4. Whether there is a flush.
     *  5. Whether there is a straight.
     *
     * @param event button "Check hand" is clicked.
     */
    public void checkHand(ActionEvent event) {
        String text = String.valueOf(model.buildString(hand));
        handStatusText.setText(text);
    }
}
