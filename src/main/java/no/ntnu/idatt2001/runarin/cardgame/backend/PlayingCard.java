package no.ntnu.idatt2001.runarin.cardgame.backend;

/**
 * Represents a playing card. A playing card has a number (face) between
 * 1 and 13, where 1 is called an Ace, 11 = Knight, 12 = Queen and 13 = King.
 * The card can also be one of 4 suits: Spade, Heart, Diamonds and Clubs.
 *
 * @author ntnu, edited by Runar Indahl
 * @version 1.0
 * @since 2022-03-28
 */
public class PlayingCard implements Comparable{

    private final String suit;  // "S"=spade, "H"=heart, "D"=diamonds, "C"=clubs
    private final int face;     // a number between 1 and 13

    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single String. "S" for Spades,
     *             "H" for Heart, "D" for Diamonds and "C" for clubs
     * @param face The face value of the card, an integer between 1 and 13
     */
    public PlayingCard(String suit, int face) {
        if (!(suit.equals("S") || suit.equals("H") || suit.equals("D") || suit.equals("C")))
            throw new IllegalArgumentException("Suit must be either 'S', 'H', 'D' or 'C'.");
        if (face < 1 || face > 13)
            throw new IllegalArgumentException("Face must be within range of 1 and 13, inclusive.");
        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit, face);
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for Clubs
     *
     * @return the suit of the card
     */
    public String getSuit() {
        return suit;
    }

    /**
     * Returns the face of the card (value between 1 and 13).
     *
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }

    /**
     * Returns the file path for this card's png-file.
     *
     * @return String path to the card's representing picture.
     */
    public String getCardPathPNG() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return "file:src\\main\\resources\\cards\\" + suit + face + ".png";
        }
        return "file:src/main/resources/cards/" + suit + face + ".png";
    }

    /**
     * compareTo-method which compares this object to another playing card in the deck.
     *
     * @param compareCard card to compare with this.
     * @return int value -1, 0 or 1, depending on if the element is before or after another in a list.
     */
    @Override
    public int compareTo(Object compareCard) {
        int compareFace = ((PlayingCard)compareCard).getFace();
        return this.face-compareFace;
    }
}
