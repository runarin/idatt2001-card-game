package no.ntnu.idatt2001.runarin.cardgame.backend;

import java.util.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Represents a deck of cards. A deck consist of 52 cards, where there are 13 cards,
 * with range from 1 to 13, for each of the suits Clubs, Hearts, Diamonds and Spades.
 *
 * @author Runar Indahl
 * @version 1.0
 * @since 2022-03-29
 */
public class DeckOfCards {

    private final String[] suit = {"S", "D", "H", "C"};
    private final int[] face = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    private final ArrayList<PlayingCard> deck;

    /**
     * Creates an instance of a DeckOfCards,
     * which contains a list with 52 individual cards.
     */
    public DeckOfCards() {
        deck = new ArrayList<>();
        Arrays.stream(suit)
                .forEach(s -> Arrays.stream(face)
                        .forEach(f -> deck.add(new PlayingCard(s, f))));
    }

    /**
     * Returns a deep copy of the card deck.
     *
     * @return the card deck as a list.
     */
    public ArrayList<PlayingCard> getDeck() {
        ArrayList<PlayingCard> deepCopy = new ArrayList<>();
        deck.forEach(card -> deepCopy.add(new PlayingCard(card.getSuit(), card.getFace())));
        return deepCopy;
    }

    /**
     * Returns hand with n number of cards.
     * Hand size can be maximum 52 cards.
     *
     * @see #getDeck() returning a copy of the original deck.
     * @param n number of cards to be added to hand.
     * @return ArrayList<PlayingCard> containing n number of cards in hand.
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (n < 0) throw new IllegalArgumentException("Cannot deal a negative number of cards to hand.");
        if (n < 1) throw new IllegalArgumentException("Must deal at least one card to hand when using this method.");
        if (n > deck.size()) throw new IllegalArgumentException("Cannot deal more cards than what is in the deck.");

        ArrayList<PlayingCard> hand = new ArrayList<>();
        ArrayList<PlayingCard> deckCopy = this.getDeck();

        Collections.shuffle(deckCopy, new Random());

        return deckCopy.stream()
                .limit(n)
                .collect(Collectors.toCollection(() -> hand));
    }
}
