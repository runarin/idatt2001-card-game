module no.ntnu.idatt2001.runarin.cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;

    opens no.ntnu.idatt2001.runarin.cardgame.frontend.controller to javafx.fxml;
    exports no.ntnu.idatt2001.runarin.cardgame.frontend.view;
}